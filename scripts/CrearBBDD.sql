
CREATE TABLE USUARIO(
Id_USUARIO int PRIMARY KEY NOT NULL,
Nombre Varchar(100) NOT NULL,
Apellidos varchar (100) NOT NULL,
Alias nvarchar(50) NOT NULL,
Email varchar(200) NOT NULL,
Telefono varchar(200) NOT NULL,
CodPostal varchar(100) NOT NULL,
Ciudad varchar(100) NOT NULL,
)
 
CREATE TABLE CATEGORIAS(
Id_Categoria int PRIMARY KEY NOT NULL,
Nombre Varchar(50) NOT NULL,
)
 
CREATE TABLE POST(
Id_POST int PRIMARY KEY NOT NULL,
Id_PostUsuario int NOT NULL,
Fecha Date NOT NULL,
Titulo varchar (100) NOT NULL,
Contenido varchar (700) NOT NULL,
Id_CategoriaPost int NOT NULL,
constraint ID_PostUsuario foreign key (ID_PostUsuario) references USUARIO(Id_USUARIO),
constraint ID_CategoriaUsuario foreign key (ID_CategoriaPost) references CATEGORIAS(Id_Categoria)
 
)